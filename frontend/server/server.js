const app = require("./index");

const server = app.listen(5000, () => {
  console.log('Server started on port 5000');
});

process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);

function shutdown() {
  server.close();
};

module.exports = server;
